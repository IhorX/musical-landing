//меню бургер
const burgerMenu = document.querySelector('.menu_burger');
if (burgerMenu){
    const menuNavbar = document.querySelector('.menu_navbar');
    const bodyNavbar = document.querySelector('.header_body');
    burgerMenu.addEventListener("click", function (){
        document.body.classList.toggle('_lock');
        burgerMenu.classList.toggle('_activ');
        menuNavbar.classList.toggle('_activ');
        bodyNavbar.classList.toggle('_activ');
    });
}


const video = document.querySelector('.section-1 .bg_video');
const playBtn = document.querySelector('.section-1 .controls_play');
const progress = document.querySelector('.section-1 .controls_progress input');
const time = document.querySelector('.section-1 .controls_timer');


// Метаданные
function videoMetaData() {
    let minutesLeft = Math.floor((video.duration - Math.floor(video.currentTime)) / 60);
    if (minutesLeft < 10) {
        minutesLeft = '0' + String(minutesLeft);
    }

    // Seconds
    let secondsLeft = Math.floor((video.duration - Math.floor(video.currentTime)) % 60);
    if (secondsLeft < 10) {
        secondsLeft = '0' + String(secondsLeft);
    }

    time.innerHTML = `00:00-${minutesLeft}:${secondsLeft}`;
}
//кнопка плей і пауза
function playVideo() {
    if (video.paused) {
        video.play();
        playBtn.classList.add('_pause')
    } else {
        video.pause();
        playBtn.classList.remove('_pause')
    }
}

// Timer
function updateProgress() {
    progress.value = (video.currentTime / video.duration) * 100;
    progress.classList.add('_thumb');

    if (time.classList.contains('_hover')) {
        // скільки пройшло
        // Minutes
        let minutes = Math.floor(video.currentTime / 60);
        if (minutes < 10) {
            minutes = '0' + String(minutes);
        }

        // Seconds
        let seconds = Math.floor(video.currentTime % 60);
        if (seconds < 10) {
            seconds = '0' + String(seconds);
        }

        // Скільки залишилось
        // Minutes
        let minutesLeft = Math.floor((video.duration - Math.floor(video.currentTime)) / 60);
        if (minutesLeft < 10) {
            minutesLeft = '0' + String(minutesLeft);
        }

        // Seconds
        let secondsLeft = Math.floor((video.duration - Math.floor(video.currentTime)) % 60);
        if (secondsLeft < 10) {
            secondsLeft = '0' + String(secondsLeft);
        }

        // На екран
        time.innerHTML = `${minutes}:${seconds}-${minutesLeft}:${secondsLeft}`;
    } else {
        // Скільки пройшло
        // Minutes
        let minutes = Math.floor(video.currentTime / 60);
        if (minutes < 10) {
            minutes = '0' + String(minutes);
        }

        // Seconds
        let seconds = Math.floor(video.currentTime % 60);
        if (seconds < 10) {
            seconds = '0' + String(seconds);
        }

        // Загальний час відео
        // Minutes
        let minutesLeft = Math.floor(video.duration / 60);
        if (minutesLeft < 10) {
            minutesLeft = '0' + String(minutesLeft);
        }

        // Seconds
        let secondsLeft = Math.floor((video.duration % 60));
        if (secondsLeft < 10) {
            secondsLeft = '0' + String(secondsLeft);
        }

        // На екран
        time.innerHTML = `${minutes}:${seconds}-${minutesLeft}:${secondsLeft}`;
    }

    const min = progress.min;
    const max = progress.max;
    const val = progress.value;

    progress.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%';
}

function setProgress() {
    video.currentTime = (progress.value * video.duration) / 100;
}
// hoverOver
function hoverOver() {
    time.classList.add('_hover');
}
// hoverOut
function hoverOut() {
    time.classList.remove('_hover');
}

video.addEventListener('loadedmetadata', videoMetaData);
playBtn.addEventListener('click', playVideo);
video.addEventListener('timeupdate', updateProgress);
progress.addEventListener('input', setProgress);
time.addEventListener('mouseover', hoverOver);
time.addEventListener('mouseout', hoverOut);



//<custom audio controls>====================================================================
const audio = document.querySelector('.section-5 .tracks_player');
const audioPlayBtn = document.querySelector('.section-5 .controls_play');
const audioProgress = document.querySelector('.section-5 .controls_progress input');
const audioTime = document.querySelector('.section-5 .controls_timer');
const audioLinks = document.querySelectorAll('.section-5 .audio_track');

let songIndex = 0;

audio.src = `audio/${audioLinks[0].innerText}.weba`;


audioLinks.forEach(element => {
    element.addEventListener('click', function () {
        if (!this.classList.contains('_active')) {
            audio.src = `audio/${this.innerText}.weba`;
            audioLinks.forEach(element => {
                element.classList.remove('_active');
            });
            this.classList.add('_active');
            playAudio();
        }
    });
});


// Метаданные
function audioMetaData() {
    let minutesLeft = Math.floor((audio.duration - Math.floor(audio.currentTime)) / 60);
    if (minutesLeft < 10) {
        minutesLeft = '0' + String(minutesLeft);
    }

    // Seconds
    let secondsLeft = Math.floor((audio.duration - Math.floor(audio.currentTime)) % 60);
    if (secondsLeft < 10) {
        secondsLeft = '0' + String(secondsLeft);
    }

    audioTime.innerHTML = `00:00-${minutesLeft}:${secondsLeft}`;
}

// Play / Pause
function playAudio() {
    if (audio.paused) {
        audio.play();
        audioPlayBtn.classList.add('_pause')
    } else {
        audio.pause();
        audioPlayBtn.classList.remove('_pause')
    }
}

function timerAudio() {
    audioProgress.value = (audio.currentTime / audio.duration) * 100;
    audioProgress.classList.add('_thumb');

    if (audioTime.classList.contains('_hover')) {
        // Сколько прошлом
        // Minutes
        let minutes = Math.floor(audio.currentTime / 60);
        if (minutes < 10) {
            minutes = '0' + String(minutes);
        }

        // Seconds
        let seconds = Math.floor(audio.currentTime % 60);
        if (seconds < 10) {
            seconds = '0' + String(seconds);
        }

        // Сколько осталось
        // Minutes
        let minutesLeft = Math.floor((audio.duration - Math.floor(audio.currentTime)) / 60);
        if (minutesLeft < 10) {
            minutesLeft = '0' + String(minutesLeft);
        }

        // Seconds
        let secondsLeft = Math.floor((audio.duration - Math.floor(audio.currentTime)) % 60);
        if (secondsLeft < 10) {
            secondsLeft = '0' + String(secondsLeft);
        }

        // Вывод
        audioTime.innerHTML = `${minutes}:${seconds}-${minutesLeft}:${secondsLeft}`;
    } else {
        // Сколько прошлом
        // Minutes
        let minutes = Math.floor(audio.currentTime / 60);
        if (minutes < 10) {
            minutes = '0' + String(minutes);
        }

        // Seconds
        let seconds = Math.floor(audio.currentTime % 60);
        if (seconds < 10) {
            seconds = '0' + String(seconds);
        }

        // Общее время видео
        // Minutes
        let minutesLeft = Math.floor(audio.duration / 60);
        if (minutesLeft < 10) {
            minutesLeft = '0' + String(minutesLeft);
        }

        // Seconds
        let secondsLeft = Math.floor((audio.duration % 60));
        if (secondsLeft < 10) {
            secondsLeft = '0' + String(secondsLeft);
        }

        // Вывод
        audioTime.innerHTML = `${minutes}:${seconds}-${minutesLeft}:${secondsLeft}`;
    }

    const min = audioProgress.min;
    const max = audioProgress.max;
    const val = audioProgress.value;

    audioProgress.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%';
}
// Progress Bar
function audioSetProgress() {
    audio.currentTime = (audioProgress.value * audio.duration) / 100;
}
// hoverOver
function audioHoverOver() {
    audioTime.classList.add('_hover');
}
// hoverOut
function audioHoverOut() {
    audioTime.classList.remove('_hover');
}

audio.addEventListener('loadedmetadata', audioMetaData);
audioPlayBtn.addEventListener('click', playAudio);
window.onload = function () {
    audio.addEventListener('timeupdate', timerAudio);
}
audioProgress.addEventListener('input', audioSetProgress);
audioTime.addEventListener('mouseover', audioHoverOver);
audioTime.addEventListener('mouseout', audioHoverOut);



